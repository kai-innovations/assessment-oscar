Debug Assignment
======

Given the following stacktrace, answer the following:

1. The most likely cause of the error based on the trace
2. How would you test to reproduce the issue
3. How would you fix the issue

```
java.lang.NullPointerException
	oscar.eform.data.EForm.putValue(EForm.java:523)
	oscar.eform.data.EForm.setValues(EForm.java:208)
	oscar.eform.actions.AddEFormAction.execute(AddEFormAction.java:184)
	...
```
